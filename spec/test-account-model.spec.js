import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";

describe('Account', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        // disable SQL logging
        process.env.NODE_ENV = 'test';
        return done();
    });
    afterAll( done => {
       if (context) {
           return context.finalize( ()=> done() );
       }
        // re-enable development environment
        process.env.NODE_ENV = 'development';
       return done();
    });

    it('should get account', async () => {
       const account = await context.model('Account').asQueryable()
           .where('name').equal('Administrators').silent().getItem();
       expect(account).toBeTruthy();
    });
});

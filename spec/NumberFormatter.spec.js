import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import {NumberFormatter} from '@universis/number-format';
// eslint-disable-next-line no-unused-vars
const executeInTransaction = TestUtils.executeInTransaction;

describe('NumberFormatter', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        return done();
    });

    it('should use NumberFormatter', async () => {
        const formatter = new NumberFormatter();
        const text = formatter.format(6.5, 'el', 2).toLocaleUpperCase('el');
        expect(text, 'ΕΞΙ ΚΑΙ ΠΕΝΗΝΤΑ ΕΚΑΤΟΣΤΑ');
    });

    it('should get student courses', async() => {
        await executeInTransaction(context, async () => {
            // get student
            const student = await context.model('Student')
                .where('studentStatus/alternateName')
                .equal('graduated')
                .and('graduationYear').equal(2015)
                .silent().getItem();
            expect(student).toBeTruthy();
            const studentCourses = await context.model('StudentCourse')
                .where('student').equal(student.id)
                .expand({
                    name: 'course/gradeScale'
                }).silent().getItems();
            expect(studentCourses).toBeTruthy();
            studentCourses.forEach( (studentCourse) => {
                if (studentCourse.isPassed) {
                    if (studentCourse.course && studentCourse.course.gradeScale) {
                        /**
                         *
                         * @type {GradeScale|*}
                         */
                        const gradeScale = context.model('GradeScale').convert(studentCourse.course.gradeScale);
                        const grade1 = gradeScale.convertTo(studentCourse.grade, true);
                        if (gradeScale.scaleType === 0) {
                            console.log('GRADE', grade1, new NumberFormatter().format(parseFloat(grade1), 'el', gradeScale.formatPrecision))
                        }
                        else {
                            console.log('GRADE', grade1);
                        }
                    }
                }
            })
        });
    });

});
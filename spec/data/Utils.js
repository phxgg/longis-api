import {INSTITUTE, DEPARTMENT, STUDY_PROGRAM, GRADE_SCALES, COURSES} from './index';
import Institute from '../../server/models/institute-model';
import StudyProgram from '../../server/models/study-program-model';
import Department from '../../server/models/department-model';
import GradeScale from "../../server/models/grade-scale-model";
import Course from "../../server/models/course-model";
import _ from 'lodash';

/**
 * @param {DataContext} context
 * @returns Promise<StudyProgram>>
 */
async function createStudyProgram(context) {
    // create institute
    await context.model(Institute).silent().insert(INSTITUTE);
    // create department
    await context.model(Department).silent().insert(DEPARTMENT);
    // add grade scales
    await context.model(GradeScale).silent().save(_.cloneDeep(GRADE_SCALES));
    // add courses
    await context.model(Course).silent().insert(COURSES);
    /**
     * @type {StudyProgram}
     */
    let studyProgram = STUDY_PROGRAM;
    // get grade scale
    studyProgram.gradeScale = await context.model(GradeScale)
        .where('name').equal('0 to 10')
        .silent().getItem();
    // create study program
    await context.model(StudyProgram).silent().insert(studyProgram);
    // add default study program specialization (core)
    return await context.model(StudyProgram)
        .where('id').equal(studyProgram.id)
        .expand('department', 'gradeScale', 'specialties')
        .silent()
        .getTypedItem();
}

export {
    createStudyProgram
};

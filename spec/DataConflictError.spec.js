import app from '../server/app';
import {DataConflictError, GenericDataConflictError} from '../server/errors';
import indexRouter from '../server/routes/index';
import request from 'supertest';

describe('DataConflictError', () => {
    it('should get conflict error', async () => {
        indexRouter.get('/error/conflict', (req, res, next) => {
            return next(new DataConflictError('An internal error occurred', null, 'Any'));
        });

        indexRouter.get('/error/anotherConflict', (req, res, next) => {
            return next(new GenericDataConflictError('ERR_CUSTOM_CONFLICT', 'An internal error occurred', null, 'Any'));
        });

        let response = await request(app)
            .get('/error/conflict')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        expect(response.statusCode).toBe(409);
        expect(response.body.code).toBe('E_CONFLICT');
        expect(response.body.message).toBe('An internal error occurred');
        expect(response.body.model).toBe('Any');
        expect(response.body.type).toBe('DataConflictError');

        response = await request(app)
            .get('/error/anotherConflict')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        expect(response.statusCode).toBe(409);
        expect(response.body.code).toBe('ERR_CUSTOM_CONFLICT');
        expect(response.body.type).toBe('GenericDataConflictError');
    });
});

import express from 'express';
import {PrivateContentService} from "../services/content-service";
import {TraceUtils,HttpServerError,HttpNotFoundError} from '@themost/common';
let router = express.Router();


/* GET home page. */
router.get('/private/:file', (req, res, next) => {
    /**
     * get private content service
     * @type {PrivateContentService}
     */
    let service = req.context.getApplication().getService(PrivateContentService);
    service.findOne(req.context, { alternateName: req.params.file }, function(err, result) {
        if (err) {
            TraceUtils.error(err);
            return next(new HttpServerError());
        }
        if (result==null) {
            return next(new HttpNotFoundError());
        }
        const fileName = result.name;
        service.resolvePhysicalPath(req.context, result, function(err, executionPath) {
            if (err) {
                return next(err);
            }
           return res.sendFile(executionPath,  (err) => {
               if (err) {
                   if (err.code === 'ENOENT') {
                       TraceUtils.error(err);
                       return next(new HttpNotFoundError());
                   }
                   return next(err);
               }
           });
        });
    });
});

module.exports = router;

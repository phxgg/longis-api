import {PercentileRanking} from "@universis/percentile-ranking";
import {HttpServerError, TraceUtils} from "@themost/common";

/**
 *
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event){
    if (event?.target?.actionStatus?.alternateName === "CompletedActionStatus" && event?.previous?.actionStatus?.alternateName !== "CompletedActionStatus"){
        const context = event?.model?.context;
        const graduationEvent = await context.model('GraduationRequestAction').where('id').equal(event?.target?.id).expand({
            "name": "student",
            "options": {
                "$expand": "department($expand=organization($expand=instituteConfiguration))"
            }
        }).getTypedItem();
        const student = graduationEvent?.student;
        // get all graduated students that enrolled the same academic year as this student
        let graduationStudents = await context.model('Students')
            .where('department').equal(student?.department.id ?? student.department)
            .and('inscriptionYear').equal(student?.inscriptionYear?.id ?? student?.inscriptionYear)
            .and('studentStatus/alternateName').equal('graduated')
            .and('id').notEqual(student?.id)
            .getTypedItems();
        let instituteConfiguration = student?.department?.organization?.instituteConfiguration;
        // if the threshold isn't satisfied get the students that enrolled in the previous academic year
        for(let i = 1; Array.isArray(graduationStudents) && graduationStudents.length < instituteConfiguration?.percentileRankItemThreshold; ++i) {
            graduationStudents = [...graduationStudents,  ...(await context.model('Students')
                .where('department').equal(student?.department.id ?? student.department)
                .and('inscriptionYear').equal((student?.inscriptionYear?.id ?? student?.inscriptionYear) - i)
                .and('studentStatus/alternateName').equal('graduated')
                .and('id').notEqual(student?.id)
                .getTypedItems()) ];
        }
        // get each student only once (probably redundant)
        let students = [student, ...graduationStudents.filter((value, index, self) => self.findIndex((m) => m.id === value.id) === index)];
        let grades = students.map(x => {return {
            grade: Math.round(x.graduationGrade*10), student: x.id, percentileRank: x.graduationRankByInscriptionYear
        }});

        switch (instituteConfiguration?.percentileRankMethod) {
            case 1:
                PercentileRanking.simplePercentileCalculation(grades);
                break;
            case 2:
                PercentileRanking.complexPercentileCalculation(grades);
                break;
            default:
                throw new HttpServerError("Percentile rank method not implemented yet.", "The percentile rank your institute has selected is not yet implemented.");
        }
        let res = [];
        // only update the student whose graduation request action got completed
        const filteredStudents = grades.filter(x=> x.student === student.id);
        for (const item of filteredStudents){
            let {id, percentileRank} = item;
            let cc = students.find(x=> x.id === item.student)
            res.push({...cc,...id, graduationRankByInscriptionYear: parseFloat(percentileRank.toFixed(4))});
        }
        try{
            return context.model('Students').silent().save(res);
        } catch (e) {
            TraceUtils.error(e);
        }
    }

}

export function afterSave(event, callback){
    afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

import _ from 'lodash';
import {ValidationResult} from "../errors";
import util from "util";
import {LangUtils, TraceUtils} from "@themost/common";
import '@themost/promise-sequence';


/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {

    const target = event.model.convert(event.target);
    const context = event.model.context;
    let classes;
    if (util.isArray(target['classes'])) {
        classes = target['classes'].filter(function (x) {
            return x.$state !== 4;
        });
        if (classes.length === 0) {
            return;
        }
        // get student
        const studentId = event.model.idOf(event.target["student"]);
        const student = await context.model('Student').where('id').equal(studentId).select('id', 'department', 'studyProgram', 'semester', 'specialtyId').flatten().silent().getTypedItem();

        // this rule will be executed only if checkCustomRules is set to true, this will be changed later on.
        const department = await context.model('LocalDepartment').where('id').equal(student.department).select('checkCustomRules').first();
        if (LangUtils.parseInt(department['checkCustomRules']) === 0) {
            return;
        }

        // get all department rules associated with courseTypes
        const departmentRules = await context.model('RequiredCourseTypeRule').where('target').equal(student.department)
            .and('targetType').equal('Department')
            .and('refersTo').equal('CourseType')
            .and('additionalType').equal('RequiredCourseTypeRule')
            .silent().getItems();

        const totalRegisteredAllowed  = {
            numberOfCourses:0,
            numberOfUnits:0,
            numberOfHours:0,
            numberOfEcts:0
        };

        if (departmentRules && departmentRules.length > 0) {
            let checkValues = [];
            for (let i = 0; i < departmentRules.length; i++) {
                if (!_.isNil(departmentRules[i].checkValues)) {
                    // check if rule refers to specific range of student semesters
                    let checkRule = true;
                    const fromSemester = LangUtils.parseInt(departmentRules[i].value1);
                    const toSemester = LangUtils.parseInt(departmentRules[i].value2);
                    if (fromSemester !== 0 || toSemester !== 0) {
                        checkRule = (fromSemester !== 0 ? (student.semester >= fromSemester ? true : false) : true) &&
                            (toSemester !== 0 ? (student.semester <= toSemester ? true : false) : true);
                    }
                    if (checkRule) {
                        checkValues = checkValues.concat(departmentRules[i].checkValues.split(','));
                     }
                }
            }
            if (checkValues.length === 0) {
                return;
            }
            const requiredCourseTypes = await context.model('CourseType').where('id').in(checkValues).getItems();
            // check if department rule uses max number of obligatory courses
            //get all available courses for courseTypes
            const studentCourses = await (await student.getAvailableClasses())
                .where('courseType').in(checkValues)
                .and('semester').lowerOrEqual(student.semester)
                .select('course')
                .orderBy('semester')
                .getItems();

            if (studentCourses.length === 0) {
                return;
            }

            let requiredCourses = [];

            // if required course is not included in current registration add this to requiredCourses
            studentCourses.forEach(studentCourse => {
                const isRegistered = classes.find((x) => {
                    return x.course === studentCourse.course;
                });
                if (!isRegistered) {
                    requiredCourses.push(studentCourse.course);
                }
            });

            if (requiredCourses.length === 0) {
                return;
            }
            // check if required courses are available, get available classes
            // get available classes for required courses
            const studentAvailableClasses = await (await student.getAvailableClasses()).where('course').in(requiredCourses).getItems();
            if (studentAvailableClasses.length === 0) {
                return;
            }

            // validate required courses - only available courses should be registered
            let coursesForRegister = [];

            // add async function for validating available class
            let forEachCourse = async availableClass => {
                try {
                    const studentCourseClass = context.model('StudentCourseClass').convert(availableClass);
                    //add registrationYear and period
                    studentCourseClass.registrationYear = target.registrationYear;
                    studentCourseClass.registrationPeriod = target.registrationPeriod;
                    return await new Promise((resolve) => {
                        studentCourseClass.validate(function (err, result) {
                            if (err) {
                                result = _.isNil(result)? [] :result;
                                if (err instanceof ValidationResult) {
                                    result.push(err);
                                } else {
                                    TraceUtils.error(err);
                                    result.push(new ValidationResult(false, 'EFAIL', context.__('An internal server error occurred.'), err.message));
                                }
                            }
                            const success = (typeof result.find(function (x) {
                                return !x.success
                            }) === 'undefined');
                            if (success) {
                                // add to required
                                // first check if replaced courses have been passed
                                const course = context.model('Course').convert(studentCourseClass.course);
                                return course.replacedPassed(studentId, function (err, passed) {
                                    if (err) {
                                         TraceUtils.error(err);
                                    } else if (!passed)
                                    {
                                        coursesForRegister.push(studentCourseClass);
                                    }
                                    return resolve();
                                });
                             }
                            return resolve();
                        });
                    });
                } catch (err) {
                    TraceUtils.error(err);
                }
            };
            // call all promises
            await Promise.sequence(studentAvailableClasses.map(availableClass => {
                return () => forEachCourse(availableClass);
            }));

            if (coursesForRegister.length > 0) {
                let message = 'The courses of types (%s) should be included into your registration';
                message =context.__(message);
                const args = [];
                const courseTypes=requiredCourseTypes.map(function(x) { return `${x.name}`; }).join(',');
                args.push(courseTypes);
                args.unshift(message);
                message= util.format.apply(this,args);

                // Not valid.  These courses should be included in registration
                // check limits from StudyProgramSemesterRules
                for (let i = 0; i < departmentRules.length; i++) {
                    const departmentRule = departmentRules[i];
                    if (LangUtils.parseInt(departmentRule.value3) !== 0) {
                        const  ignoreSemester = LangUtils.parseInt(departmentRule.value4);
                        // check also limits from StudyProgramSemesterRules
                        // get studyProgramSemesterRules for student semester
                        if (event.target.semesterRules && event.target.semesterRules.length > 0) {
                            for (let j = 0; j < event.target.semesterRules.length; j++) {
                                const ruleTypes = departmentRule.checkValues.split(',');
                                const semesterRule = event.target.semesterRules[j];

                                if (semesterRule.courseTypes.length === 0 || (semesterRule.courseTypes.some(r => ruleTypes.indexOf(r.id) >= 0))) {
                                    totalRegisteredAllowed.numberOfCourses = semesterRule.courses;
                                    totalRegisteredAllowed.numberOfUnits = semesterRule.units;
                                    totalRegisteredAllowed.numberOfHours = semesterRule.hours;
                                    totalRegisteredAllowed.numberOfEcts = semesterRule.ects;
                                }

                                if (totalRegisteredAllowed.numberOfEcts !== 0 || totalRegisteredAllowed.numberOfCourses !== 0 ||
                                    totalRegisteredAllowed.numberOfUnits !== 0 || totalRegisteredAllowed.numberOfHours !== 0
                                ) {
                                    // filter coursesForRegister
                                    // get all courses with semester less than max semester of registered courses

                                    const maxRegisteredSemester = Math.max(...classes.filter(x => {
                                        return ruleTypes.findIndex(z => (x.courseType) == z) > -1;
                                    }).map(x => (x.semester && x.semester.id)));

                                    const coursesForRegisterBySemester = coursesForRegister.filter(x => {
                                        return x.semester.id < maxRegisteredSemester;
                                    });
                                    if (coursesForRegister.length > 0) {
                                        // check if registered courses have courseType and calculate sums
                                        const registeredCourses = classes.filter(x => {
                                            return ruleTypes.findIndex(z => (x.courseType) == z) > -1;
                                        });
                                        registeredCourses.sumOfEcts = registeredCourses.reduce((partial_sum, a) => partial_sum + a.ects, 0);
                                        registeredCourses.sumOfUnits = registeredCourses.reduce((partial_sum, a) => partial_sum + a.units, 0);
                                        registeredCourses.sumOfHours = registeredCourses.reduce((partial_sum, a) => partial_sum + a.hours, 0);

                                        totalRegisteredAllowed.numberOfCourses = totalRegisteredAllowed.numberOfCourses>=registeredCourses.length? totalRegisteredAllowed.numberOfCourses - registeredCourses.length: totalRegisteredAllowed.numberOfCourses;
                                        totalRegisteredAllowed.numberOfEcts = totalRegisteredAllowed.numberOfEcts>=registeredCourses.sumOfEcts? totalRegisteredAllowed.numberOfEcts- registeredCourses.sumOfEcts: totalRegisteredAllowed.numberOfEcts;
                                        totalRegisteredAllowed.numberOfUnits = totalRegisteredAllowed.numberOfUnits>=registeredCourses.sumOfUnits? totalRegisteredAllowed.numberOfUnits- registeredCourses.sumOfUnits: totalRegisteredAllowed.numberOfUnits;
                                        totalRegisteredAllowed.numberOfHours = totalRegisteredAllowed.numberOfHours>=registeredCourses.sumOfHours? totalRegisteredAllowed.numberOfHours- registeredCourses.sumOfHours: totalRegisteredAllowed.numberOfHours;

                                        if (coursesForRegisterBySemester.length > 0 && !ignoreSemester) {
                                            const courses = coursesForRegisterBySemester.map(function (x) {
                                                return `(${x.displayCode}) ${x.name}`;
                                            }).join(', ');
                                            message = `${context.__('The registration is not valid.')} ${context.__('Not passed courses from previous semesters must be included in registration')}: ${courses}`;
                                            // return validationResult

                                            const validationResult = Object.assign(new ValidationResult(false, 'EFAIL', message, null, coursesForRegister), {
                                                type: 'RequiredCourseTypeRule'
                                            });
                                            return validationResult;
                                        }

                                        const totalRequiredFromPreviousSemesters = LangUtils.parseInt(departmentRule.value5);
                                        if (totalRequiredFromPreviousSemesters>0) {
                                            // filter required courses from previous semesters
                                            const registeredPreviousSemesters = classes.filter(x => {
                                                return x.semester.id < student.semester && ruleTypes.findIndex(z => (x.courseType) == z) > -1;
                                            });
                                            const totalRemainingPreviousSemesters = coursesForRegister.filter(x => {
                                                return x.semester.id < student.semester && ruleTypes.findIndex(z => (x.courseType.id) == z) > -1;
                                            });
                                            if (totalRequiredFromPreviousSemesters > registeredPreviousSemesters.length && totalRemainingPreviousSemesters.length) {
                                                message = `${context.__('The registration is not valid.')} ${context.__('Not passed courses from previous semesters must be included in registration')}. ${context.__('Minimum number of courses from previous semesters to be registered -if any-')} ${totalRequiredFromPreviousSemesters}`;
                                                // return validationResult

                                                const validationResult = Object.assign(new ValidationResult(false, 'EFAIL', message, null, coursesForRegister), {
                                                    type: 'RequiredCourseTypeRule'
                                                });
                                                return validationResult;
                                            }
                                            return;
                                        }

                                        if (totalRegisteredAllowed.numberOfCourses > 0 || totalRegisteredAllowed.numberOfEcts>0 || totalRegisteredAllowed.numberOfUnits>0 || totalRegisteredAllowed.numberOfHours>0) {
                                            // Not valid.  Some of these courses should be included in registration
                                            const courses = coursesForRegister.map(function (x) {
                                                return `(${x.displayCode}) ${x.name}`;
                                            }).join(', ');
                                            let messageDescription = totalRegisteredAllowed.numberOfCourses>0 ? `${totalRegisteredAllowed.numberOfCourses} ${context.__('courses')}`:'';
                                            messageDescription += totalRegisteredAllowed.numberOfEcts>0 ? `${totalRegisteredAllowed.numberOfEcts} ${context.__('ECTS')}`:'';
                                            messageDescription += totalRegisteredAllowed.numberOfUnits>0 ? `${totalRegisteredAllowed.numberOfUnits} ${context.__('Units')}`:'';
                                            messageDescription += totalRegisteredAllowed.numberOfHours>0 ? `${totalRegisteredAllowed.numberOfHours} ${context.__('Hours')}`:'';
                                            message = `${context.__('The registration is not valid.')} ${messageDescription} ${context.__('must be included of the following')} : ${courses}`;
                                            // return validationResult
                                            const validationResult = Object.assign(new ValidationResult(false, 'EFAIL', message, null, coursesForRegister), {
                                                type: 'RequiredCourseTypeRule'
                                            });
                                            return validationResult;
                                        }
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }

                const courses= coursesForRegister.map(function(x) { return `(${x.displayCode}) ${x.name}`; }).join(', ');
                message = util.format.apply(this,args);
                // return validationResult
                const validationResult = Object.assign(new ValidationResult(false, 'EFAIL', message, courses, coursesForRegister), {
                    type: 'RequiredCourseTypeRule'
                });
                return validationResult;
            }
        }
        return;
    }
    return;
}


export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    if (event.state !== 2 && event.state!==1) {
        return callback();
    }
    // execute async method
    return afterSaveAsync(event).then((validationResult) => {
        event.target.validationResult = validationResult;
        if (validationResult && !validationResult.success)
        {
            return callback(validationResult);
        }
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

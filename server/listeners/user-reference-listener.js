import { DataError, DataNotFoundError } from "@themost/common";
import { DataObjectState } from "@themost/data";

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    // get context
    const context = event.model.context;
    if (event.state === DataObjectState.Insert) {
        if (!(event.target.groups && Array.isArray(event.target.groups) && event.target.groups.length)) {
            throw new DataError('Groups need to be provided, so the application user can be created.');
        }
        const groupId = event.target.groups[0].id || event.target.groups[0];
        // validate group
        const group = await context.model('Group')
            .where('id').equal(groupId)
            .select('name')
            .silent()
            .value();
        if (group == null) {
            throw new DataNotFoundError('The provided group cannot be found.');
        }
        // set applicationId ('GRUNI' must be set for Students group)
        const applicationId = group === 'Students' ? 'GRUNI' : 'GRWEB';

        // Note: Db trigger is used at this context
        const applicationUser = {
            userId: event.target.id,
            applicationId: applicationId,
            roleId: groupId
        };
        // save applicationUser
        await context.model('ApplicationUser').silent().save(applicationUser);
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

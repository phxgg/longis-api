import util from 'util';
import {LangUtils,TraceUtils} from '@themost/common/utils';
import _ from 'lodash';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    try {
        if (event.state !== 2) return callback();
        const context = event.model.context;
        /**
         * @type {CourseExamDocument|DataObject}
         */
        const target = event.target;
        context.model('CourseExamDocument').where("id").equal(target.id).flatten().first(function (err, result) {
            if (err) {
                callback(err);
            }
            else {
                event.previous = result;
                callback();
            }
        });
    }
    catch (e) {
        callback(e)
    }
}

/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function afterSave(event, callback) {
    try {
        if (event.state !== 2) return callback();
        const context = event.model.context, target = event.target;

        const status = target.documentStatus ? target.documentStatus.alternateName: null;
        let eventTitle = '';
        const prevStatus = event.previous["documentStatus"] || 0;
        //add to event but first get course exam doc from db
        const courseExamDocumentModel = context.model('CourseExamDocument');
        if (status === 'cancelled' && prevStatus === 1) {
            //document is cancelled
            courseExamDocumentModel.where('id').equal(target.id).expand('courseExam').silent().first(function (err, doc) {
                if (err) {
                    return callback(err);
                }
                if (!_.isNil(doc)) {

                    courseExamDocumentModel.resolveMethod('me', [], function (err, user) {
                        if (err) {
                            return callback(err);
                        }
                        const query = `sp_cancelCourseExamDocumentAndRevertGrades ${target.id},${user}`;
                        context.db.execute(query, null, function (err, result) {
                            if (err) {
                                TraceUtils.error(err);
                                return callback(err);
                            }
                            else {
                                context.unattended(function (cb) {
                                    eventTitle = util.format('Ακύρωση βαθμολογίου [%s] της εξεταστικής περιόδου [%s] %s %s-%s %s.'
                                        , doc.id, doc.courseExam.id, doc.courseExam.name, doc.courseExam.year, doc.courseExam.year + 1, doc.courseExam.examPeriod);
                                    context.model('EventLog').save({
                                        title: eventTitle,
                                        eventType: 2,
                                        username: context.interactiveUser.name,
                                        eventSource: 'uniAdmSys',
                                        eventApplication: 'GRWEB'
                                    }, function (err) {
                                        cb(err);
                                    });
                                }, function (err) {
                                    if (err) {
                                        TraceUtils.error(err);
                                    }
                                    callback(err);
                                });
                            }
                        });
                    });
                }
                else {
                    callback();
                }
            });
        }
        else {

            callback();

        }
    }
    catch (e) {
        callback(e);
    }
}

# SendEmailAfterMessage

Send email after a new message

### Instalation

SendEmailAfterMessage must be registered in application configuration services:

        "services": [
                ...
                {
                    "serviceType": "./listeners/send-email-document-listener#EmailDocumentAction" 
                }
                ...
            ]
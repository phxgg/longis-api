import {ValidationResult} from "../errors";
import {TraceUtils} from "@themost/common/utils";
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterExecute(event, callback) {
    let data = event['result'];
    // validate data
    if (data == null) {
        return callback();
    }
    // validate query and exit
    if (event.query.$group) {
        return callback();
    }
    if (!(Array.isArray(data) && data.length))
    {
        return callback();
    }

    const context = event.model.context;
    (async function () {
        // get student department
        const student = await context.model('Student').where('user/name').equal(context.user.name).silent().getItem();
        if (student == null) {
            return;
        }
        let forEachConfig = (request) => {
            try {
                return new Promise((resolve) => {
                    const requestConfiguration = context.model('StudentRequestConfiguration').convert(request);
                    requestConfiguration.validate(function (err, result) {
                        if (err) {
                            if (err instanceof ValidationResult) {
                                requestConfiguration.validationResult = err;
                            } else {
                                TraceUtils.error(err);
                                requestConfiguration.validationResult = new ValidationResult(false, 'EFAIL', context.__('An internal server error occurred.'), err.message);
                            }
                        }
                        request.validationResult = result;
                        return resolve();
                    });
                });

            } catch (err) {
                TraceUtils.error(err);
            }
        };
        for (let i = 0; i < data.length; i++) {
            const requestConfiguration = data[i];
            await forEachConfig(Object.assign(requestConfiguration, {
                student
            }));
        }
        // delete student attribute
        data.forEach((requestConfiguration) => delete requestConfiguration.student);
    })().then(() => {
        // filter only request configuration with success validation result
        data.filter(x => {
            return x.validationResult && x.validationResult.success === true;
        });
        return callback();
    }).catch( err => {
        TraceUtils.error(err);
        return callback();
    });

}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */

import {GenericDataConflictError} from "../errors";
import {DataModel} from "@themost/data";

export function beforeSave(event, callback) {
    return CourseClassSectionEventListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

export function beforeRemove(event, callback) {
    return CourseClassSectionEventListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return CourseClassSectionEventListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

class CourseClassSectionEventListener {

    static async beforeSaveAsync(event) {
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        if (event.state === 1) {
            if (event.target.courseClass) {
                // get courseClassSections
                let lastSection = await context.model('CourseClassSection')
                    .where('courseClass').equal(event.target.courseClass)
                    .select('max(section) as section').value();

                event.target.section = (lastSection || 0) + 1;
                event.target.sectionIndex = event.target.secIndex || event.target.section;
                // remove zero values from instructors, ensure association
                event.target.instructor1 = event.target.instructor1 || null;
                event.target.instructor2 = event.target.instructor2 || null;
                event.target.instructor3 = event.target.instructor3 || null;
            } else {
                throw new GenericDataConflictError('E_COURSE_CLASS_MISSING', context.__('Course class is missing'), null, 'CourseClassSection');
            }
        }

    }

    static async beforeRemoveAsync(event) {
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        const section = await context.model('CourseClassSection').where('id').equal(event.target.id).silent().getItem();
        // check student registrations
        const students = await context.model('StudentCourseClass')
            .where('courseClass').equal(section.courseClass)
            .and('section').equal(section.section)
            .select('count(id) as total').silent().value();
        if (students && students > 0) {
            throw new GenericDataConflictError('E_STUDENT_REGISTERED', context.__('Course class section is registered by students'), null, 'CourseClassSection');
        }
        const model=event.model;
        const getReferenceMappings = DataModel.prototype.getReferenceMappings;
        model.getReferenceMappings   = async function () {
            const res = await getReferenceMappings.bind(this)();
            // remove readonly model CourseClassSectionInstructor from mapping before delete
            const mappings = ['CourseClassSectionInstructor'];
            return res.filter((mapping) => {
                return mappings.indexOf(mapping.childModel)<0;
            });
        };
        // check if class section has rules and remove them too.
        const target = model.convert(event.target);
        const rules = await target.getCourseClassSectionRules();
        if (rules && rules.length) {
            await context.model('CourseClassSectionRule').silent().remove(rules);
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        if (event.state === 1) {
            // add also rules
            const target = event.model.convert(event.target);
            if (target.rules && target.rules.length) {
                // add also courseClass rules
                // check if rules refer to other class section
                const copyRules = target.rules.find(x => {
                    return x.target !== target.id;
                });
                if (copyRules) {
                    //save rules
                    // check if courseClass section has complex rules expression
                    const hasComplexRules = target.rules.find(x => {
                        return x.ruleExpression != null;
                    });
                    if (hasComplexRules) {
                        // ruleExpression should be replaced from new ids
                        let ruleExpression = target.rules[0].ruleExpression;
                        let newRules = [];
                        //each rule should be saved to get new id and replace it to ruleExpression
                        for (let i = 0; i < target.rules.length; i++) {
                            const rule = target.rules[i];
                            const oldId = rule.id;
                            delete rule.id;
                            // save new rule
                            const result = await target.setCourseClassSectionRules([rule]);
                            const newRule = result[(result.length - 1)];
                            newRules.push(newRule);
                            ruleExpression = ruleExpression.replace(new RegExp(`\\[%${oldId}\\]`, 'g'), `[%${newRule.id}]`);
                        }
                        // update ruleExpression
                        newRules = newRules.map(x => {
                            x.ruleExpression = ruleExpression;
                            return x;
                        });
                        // save rules
                        await target.setCourseClassSectionRules(newRules);
                    } else {
                        // remove id and save
                        target.rules.map(x => {
                            delete x.id;
                        });
                        await target.setCourseClassSectionRules(target.rules);
                    }
                }
            }
        }
    }
}

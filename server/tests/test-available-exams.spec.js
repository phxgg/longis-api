import {assert} from 'chai';
import app from '../app';
import Student from '../models/student-model';
import {TestUtils} from '../utils';


describe('test graduation rules', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const _app = app.get('ExpressDataApplication');
        // create context
        context = _app.createContext();
        // set current student
        // user with success: 'user_20100248@example.com'
        // user with success and complex rules: 'user_20131066@example.com'
        // user failure: 'user_20138601@example.com'
        context.user = {
            name: 'user_20138601@example.com'
        };
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
        context.finalize(() => {
            return done();
        });
    });

    it('should get all available course exams', async ()=> {
        /**
         * @type Student
         */
        const student = await Student.getMe(context).getTypedItem();
        const data = {
            student: student
        };
        const availableCourseExams =await student.getAvailableExamsForParticipation();
        assert.isArray(availableCourseExams);

    });


    it('should add student exam participation action', async () =>  {

        /**
         * @type Student
         */
        let student = await context.model('Student').where('user/name').equal(context.user.name).getTypedItem();
        await TestUtils.executeInTransaction(context, async () => {
            // get available course exams
            const availableCourseExams =await student.getAvailableExamsForParticipation();

            const participateAction ={
                student: student.id,
                year: 2017,
                examPeriod: 20000004,
                courseExamActions:[]
            };
            availableCourseExams.forEach(courseExam=>{
               participateAction.courseExamActions.push({
                   "courseExam": courseExam.id,
                   "student": student.id,
                   "agree": (courseExam.id % 2) === 0 ? 0 : 1
              });
            });
            // save register action
            await context.model('ExamPeriodParticipateAction').save(participateAction);

        });
    });
});

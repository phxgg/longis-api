
import app from '../app';
import {assert} from 'chai';
import {ExpressDataApplication} from '@themost/express';
import { SendEmailAfterMessage } from '../listeners/send-email-after-message-listener';
import {TestUtils} from '../utils';

describe('SendEmailAfterMessage', () => {
    before(done => {
        context = app.get(ExpressDataApplication.name).createContext();
        context.getApplication().useService(SendEmailAfterMessage);
       return done();
    });
    after(done => {
        if (context) {
            context.finalize(() => {
                return done();
            });
        }
    });
    it('should get service', () => {
        const service = context.getApplication().getService(SendEmailAfterMessage);
        assert.isDefined(service);
    });
    it('should send message', async () => {
        await TestUtils.executeInTransaction(context,async ()=> {

            const instructor = await context.model('Instructor')
                .where('isTeaching').equal(true)
                .and('user').notEqual(null)
                .expand('user')
                .silent()
                .getItem();

            context.user = {
                name: instructor.user.name
            };

            const student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('person/email').notEqual(null)
                .and('user').notEqual(null)
                .and('semester').lowerThan(8)
                .silent()
                .getItem();
        

        try {
            await context.model('StudentMessage').silent().save({
                student: student,
                action: null,
                subject: "Hello",
                body: "Hello World"
            });
        } catch (error) {
            console.log('error ',JSON.stringify(error,null,4))
            throw error;
        }
            
        });
    })
});
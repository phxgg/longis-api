// import intl extensions
import '../extensions/intl-extensions';
// import data model extensions for adding additional query options in system queries
// e.g. /api/courseClasses/?$filter=year eq currentYear() and period eq currentPeriod()
import '../extensions/data-model-extensions';
// import function context extensions for calculating values for data model attributes
// e.g.
// {
//     "@id": "http://universis.io/schemas/dueDate",
//     "name": "dueDate",
//     "description": "The due date of something.",
//     "type": "DateTime",
//     "value": "javascript:return this.today();"
// }
import '../extensions/functions-context-extensions';
// import data context extensions (localization etc)
import '../extensions/data-context-extensions';
import {ScopeAccessConfiguration, DefaultScopeAccessConfiguration} from "../services/scope-access-configuration";
import {ExpressDataApplication} from "@themost/express";
import path from "path";
import i18n from "i18n";

// data context setup
// https://github.com/kbarbounakis/most-data-express#Usage
const dataApplication = new ExpressDataApplication(path.resolve(__dirname, '../config'));

// set OAuth2 scope access configuration strategy
dataApplication.getConfiguration().useStrategy(ScopeAccessConfiguration, DefaultScopeAccessConfiguration);

// configure i18n
// https://github.com/mashpie/i18n-node#configure
// get locale configuration
let appLocaleConfiguration = dataApplication.getConfiguration().getSourceAt('settings/i18n') || {
    locales: ['en'],
    defaultLocale: 'en'
};
// finally configure i18n
i18n.configure({
    locales: appLocaleConfiguration.locales,
    defaultLocale: appLocaleConfiguration.defaultLocale,
    directory: path.resolve(process.cwd(), 'locales')
});
module.exports = dataApplication;

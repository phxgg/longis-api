import {assert} from 'chai';
import app from '../app';
import {TestUtils} from '../utils';
import moment from 'moment';

describe('model update permissions', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    // dgkimpir,fxyloudi
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const _app = app.get('ExpressDataApplication');
        // create context
        context = _app.createContext();
        // set current student
        context.user = {
            name: 'registrar2@example.com'
        };
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
        context.finalize(() => {
            return done();
        });
    });

    it('update studyProgram', async ()=> {
        /**
         * get inactive StudyProgram
         * @type {StudyProgram}
         */
        const program = await context.model('StudyProgram')
            .where('id').equal(260000055).select('id','name').getItem();

        program.name='test - ' + program.name;
        program.isActive = true;
        await context.model('StudyProgram').save(program).catch(err=>{
           assert.ifError(err,err.message);
       });

        assert.equal('Saved', 'Saved');
    });

    it('update course', async ()=> {
        /**
         * get course
         * @type {Course}
         */
        const course = await context.model('Course')
            .where('id').equal(20002563).select('id','name').getItem();

        course.name='test - ' + course.name;
        course.isEnabled = true;
        await context.model('Course').save(course).catch(err=>{
            assert.ifError(err,err.message);
        });

        assert.equal('Saved', 'Saved');
    });

    it('update course class', async ()=> {
        /**
         * get course
         * @type {CourseClass}
         */
        const courseClass = await context.model('CourseClass')
            .where('id').equal(20071822).select('id','title').getItem();

        courseClass.title='test - ' + courseClass.title;
        courseClass.mustRegisterSection = true;
        await context.model('CourseClass').save(courseClass).catch(err=>{
            assert.ifError(err,err.message);
        });

        assert.equal('Saved', 'Saved');
    });

    it('add course class instructor', async ()=> {
        /**
         * get courseClassInstructor
         * @type {CourseClassInstructor}
         */
        const courseClassInstructor = {
            "instructor":20000019,
            "courseClass":20071822,
            "allowEdit":true
        }

        //save
        await context.model('CourseClassInstructor').save(courseClassInstructor).catch(err=>{
            assert.ifError(err,err.message);
        });
        assert.equal('Saved', 'Saved');
    });


    it('update course exam', async ()=> {
        /**
         * get courseExam
         * @type {CourseExam}
         */
        const courseExam = await context.model('CourseExam')
            .where('id').equal(20115145).select('id','name').getItem();

        courseExam.name='test - ' + courseExam.name;
        courseExam.isLate = false;
        await context.model('CourseExam').save(courseExam).catch(err=>{
            assert.ifError(err,err.message);
        });

        assert.equal('Saved', 'Saved');
    });

    it('add course exam class', async ()=> {
        /**
         * add courseExamClass
         * @type {CourseExamClass}
         */
        const courseExamClass = {
            "courseExam":20115145,
            "courseClass":20076423
        }

        //save
        await context.model('CourseExamClass').save(courseExamClass).catch(err=>{
            assert.ifError(err,err.message);
        });
        assert.equal('Saved', 'Saved');
    });

    it('add course exam instructor', async ()=> {
        /**
         * add CourseExamInstructor
         * @type {CourseExamInstructor}
         */
        const courseExamInstructor = {
            "courseExam":20115145,
            "instructor":20000018
        }

        //save
        await context.model('CourseExamInstructor').save(courseExamInstructor).catch(err=>{
            assert.ifError(err,err.message);
        });
        assert.equal('Saved', 'Saved');
    });


    it('add instructor', async ()=> {
        /**
         * get Instructor
         * @type {Instructor}
         */
        const instructor = {
            'familyName' : 'Smith',
            'givenName' : 'Mary',
            "department":420
        }

        await context.model('Instructor').save(instructor).catch(err=>{
            assert.ifError(err,err.message);
        });

        assert.equal('Saved', 'Saved');
    });


    it('update instructor', async ()=> {
        /**
         * get Instructor
         * @type {Instructor}
         */
        const instructor = await context.model('Instructor').where('id').equal('20000373').expand('status').getItem();

        instructor.familyName = 'ΠΕΤΡΟΥ';
        await context.model('Instructor').save(instructor).catch(err => {
            assert.ifError(err, err.message);
        });

        assert.equal('Saved', 'Saved');
    });


    it('update student', async ()=> {
        /**
         * get student
         * @type {student}
         */
        const student = await context.model('Student').where('id').equal('20013420').expand('person').getItem();

        student.person.fatherName = 'ΠΕΤΡΟΣ';
        await context.model('Student').save(student).catch(err => {
            assert.ifError(err, err.message);
        });

        assert.equal('Saved', 'Saved');
    });

    it('update scholarship', async ()=> {
        /**
         * get Instructor
         * @type {Scholarship}
         */
        const scholarship = await context.model('Scholarship').where('id').equal('20000240').expand('organization').getItem();

        scholarship.organization = 20000002;
        await context.model('Scholarship').save(scholarship).catch(err => {
            assert.ifError(err, err.message);
        });

        assert.equal('Saved', 'Saved');
    });

    it('update internship', async ()=> {
        /**
         * get Instructor
         * @type {Scholarship}
         */
        const internship = await context.model('Internship').where('id').equal('20000004').expand('company').getItem();

        internship.company = 20000002;
        await context.model('Internship').save(internship).catch(err => {
            assert.ifError(err, err.message);
        });

        assert.equal('Saved', 'Saved');
    });

    it('add student scholarship', async ()=> {
        /**
         * @type {Scholarship}
         */
        const scholarship = {};
        scholarship.name = 'Testing scholarship 2017-2018';
        scholarship.department = 420;
        scholarship.totalStudents = 10;
        scholarship.organization = await context.model('Company').select('id').getItem();
        scholarship.scholarshipType = await context.model('ScholarshipType').select('id').getItem();
        scholarship.year=2017;

        const student = await context.model('Student')
            .where('studentStatus/alternateName').equal('active').and('inscriptionYear').greaterThan('2015').select('id').getItem();

        try {
            // save scholarship
            await context.model('Scholarship').save(scholarship);
            // add student Scholarship
            /**
             * @type {StudentScholarship}
             */
            const studentScholarship = {};
            studentScholarship.scholarship = scholarship;
            studentScholarship.student = student;
            studentScholarship.dateCompleted = new Date();
            studentScholarship.grade = 0.891;
            studentScholarship.amount = 1200;
            await context.model('StudentScholarship').save(studentScholarship);
            assert.equal('Saved', 'Saved');
        } catch (e) {

            assert.ifError(e, e.message);
        }
    });

    it('add student thesis', async ()=> {

        /**
         * @type {Student}
         */
        const student = await context.model('Student')
            .where('studentStatus/alternateName').equal('active').and('inscriptionYear').greaterThan('2015')
            .and('department').equal('420').select('id').getItem();


        /**
         * @type {Thesis}
         */
        const thesis = {};
        thesis.name = 'Testing thesis 2017-2018';
        thesis.instructor=await context.model('Instructor').select('id').getItem();
        thesis.department=420;
        thesis.startYear=2017;
        thesis.startPeriod=1;
        thesis.gradeScale= await context.model('GradeScale').select('id').getItem();
        thesis.type=await context.model('ThesisType').select('id').getItem();

        try {
            // save thesis
            await context.model('Thesis').save(thesis);
            // add student thesis
            /**
             * @type {StudentThesis}
             */
            const studentThesis = {};
            studentThesis.thesis = thesis;
            studentThesis.student = student;
            studentThesis.dateCompleted = new Date();
            await context.model('StudentThesis').save(studentThesis);

            // add student thesis result
            /**
             * @type {StudentThesisResult}
             */
            const studentThesisResult={};


            assert.equal('Saved', 'Saved');
        } catch (e) {

            assert.ifError(e, e.message);
        }
    });


});

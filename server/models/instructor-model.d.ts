import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Gender = require('./gender-model');
import Department = require('./department-model');
import Country = require('./country-model');
import User = require('./user-model');

/**
 * @class
 */
declare class Instructor extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός προσωπικού
      */
     public id: number; 
     
     /**
      * @description Επώνυμο
      */
     public familyName: string; 
     
     /**
      * @description Όνομα
      */
     public givenName: string; 
     
     /**
      * @description Τίτλος (προσφώνηση)
      */
     public title?: string; 
     
     /**
      * @description Μεσαίο όνομα
      */
     public middleName?: string; 
     
     /**
      * @description Φύλο
      */
     public gender?: Gender|any; 
     
     public personCode?: string; 
     
     /**
      * @description Κατάσταση
      */
     public status?: number; 
     
     /**
      * @description Κατηγορία
      */
     public category?: string; 
     
     /**
      * @description Διδάσκει (ΝΑΙ/ΟΧΙ)
      */
     public isTeaching?: boolean; 
     
     public expertise?: string; 
     
     /**
      * @description Ειδικότητα
      */
     public specialty?: string; 
     
     /**
      * @description Το τμήμα που ανήκει ο διδάσκων
      */
     public department?: Department|any; 
     
     /**
      * @description Διεύθυνση κατοικίας
      */
     public homeAddress?: string; 
     
     /**
      * @description ΤΚ διεύθυνσης κατοικίας
      */
     public homePostalCode?: string; 
     
     /**
      * @description Πόλη κατοικίας
      */
     public homeCity?: string; 
     
     /**
      * @description Κωδικός χώρας κατοικίας
      */
     public country?: Country|any; 
     
     /**
      * @description Τηλέφωνο κατοικίας
      */
     public homePhone?: string; 
     
     /**
      * @description Διεύθυνση εργασίας
      */
     public workAddress?: string; 
     
     /**
      * @description ΤΚ διεύθυνσης εργασίας
      */
     public workPostalCode?: string; 
     
     /**
      * @description Πόλη εργασίας
      */
     public workCity?: string; 
     
     /**
      * @description Κωδικός χώρας εργασίας
      */
     public workCountry?: Country|any; 
     
     /**
      * @description Διεύθυνση ηλεκτρονικού ταχυδρομείου επικοινωνίας
      */
     public email?: string; 
     
     /**
      * @description Αριθμός fax
      */
     public fax?: string; 
     
     /**
      * @description Τηλέφωνο εργασίας
      */
     public workPhone?: string; 
     
     /**
      * @description Ηλεκτρονική διεύθυνση
      */
     public url?: string; 
     
     public instructorCode?: string; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date; 
     
     /**
      * @description Ο συνδεδεμένος χρήστης
      */
     public user?: User|any; 

}

export = Instructor;
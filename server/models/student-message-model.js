import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
let Message = require('./message-model');
/**
 * @class
 
 * @property {Student|any} student
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('StudentMessage')
class StudentMessage extends Message {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = StudentMessage;
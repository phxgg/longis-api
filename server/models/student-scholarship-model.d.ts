import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Scholarship = require('./scholarship-model');
import Student = require('./student-model');

/**
 * @class
 */
declare class StudentScholarship extends DataObject {

     
     /**
      * @description Id
      */
     public id: number; 
     
     /**
      * @description Υποτροφία
      */
     public scholarship: Scholarship|any; 
     
     /**
      * @description Φοιτητής
      */
     public student: Student|any; 
     
     /**
      * @description Ημερομηνία ολοκλήρωσης
      */
     public dateCompleted?: Date; 
     
     /**
      * @description Σημειώσεις
      */
     public notes?: string; 
     
     /**
      * @description Ημερομηνία τροποποίησης
      */
     public dateModified: Date;

     /**
      * @grade Βαθμός
      */
     public grade: number;

     /**
      * @amount Ποσό
      */
     public amount: number;
}

export = StudentScholarship;

import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
/**
 * @class
 */
declare class Nationality extends DataObject {

     
     /**
      * @description Id
      */
     public id: number; 
     
     /**
      * @description Η ονομασία της υπηκοότητας.
      */
     public name: string; 

}

export = Nationality;
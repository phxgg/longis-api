import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {string} additionalType
 * @property {string} alternateName
 * @property {string} contentType
 * @property {Date} datePublished
 * @property {boolean} published
 * @property {string} keywords
 * @property {string} thumbnail
 * @property {string} version
 * @property {AttachmentType|any} attachmentType
 * @property {User|any} owner
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 * @augments {DataObject}
 */
@EdmMapping.entityType('Attachment')
class Attachment extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Attachment;
import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import {promisify} from "es6-promisify";
import {DataObjectState, DataPermissionEventListener} from "@themost/data";
const Rule = require('./rule-model');

/**
 * @class
 
 * @property {number} id
 * @property {string} name
 * @property {Department|any} department
 * @property {Student|any} student
 * @property {Company|any} company
 * @property {string} companyContact
 * @property {string} companyContactPhone
 * @property {AcademicYear|any} internshipYear
 * @property {AcademicPeriod|any} internshipPeriod
 * @property {Date} internshipDate
 * @property {Date} startDate
 * @property {Date} endDate
 * @property {InternshipStatus|any} status
 * @property {Date} dateCompleted
 * @property {Date} dateModified
 * @augments {DataObject}
 */
@EdmMapping.entityType('Internship')
class Internship extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

}
module.exports = Internship;

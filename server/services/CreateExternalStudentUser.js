import { TraceUtils, DataError, DataNotFoundError } from "@themost/common";
import { DataObjectState, DataPermissionEventListener, PermissionMask } from "@themost/data";
import { DataConflictError } from "../errors";
import { getMailer } from "@themost/mailer";
import { CreateExternalStudentUserStrategy } from "./CreateExternalStudentUserStrategy";
import {promisify} from "es6-promisify";



/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	if (event.state !== DataObjectState.Insert) {
		return;
	}
	// get validator listener
	const validator = new DataPermissionEventListener();
	// noinspection JSUnresolvedFunction
	const validateAsync = promisify(validator.validate)
		.bind(validator);
	// validate Student/SendActivationMessage execute permission
	const validateEvent = {
		model: event.model.context.model('Student'),
		privilege: 'Student/CreateExternalUser',
		mask: PermissionMask.Execute,
		target: event.target && event.target.student,
		throwError: true
	}
	await validateAsync(validateEvent);
	const context = event.model.context;
	const studentId =
		typeof event.target.student === "object"
			? event.target.student.id
			: event.target.student;
	// get student
	const student = await context
		.model("Student")
		.where("id")
		.equal(studentId)
		.expand("user", "person")
		.getItem();
	if (student == null) {
		throw new DataNotFoundError(
			"The specified student cannot be found or is inaccesible"
		);
	}
	if (student.user == null) {
		throw new DataError(
			"E_NOENT",
			"The student user has not yet been created or is inaccessible",
			null,
			"CreateStudentUserAction"
		);
	}
	// get oauth2 client service
	const service = context
		.getApplication()
		.getService(function OAuth2ClientService() {});
	// validate createUser and getUser functions
	if (
		typeof service.createUser !== "function" ||
		typeof service.getUser !== "function"
	) {
		throw new DataConflictError(
			"The operation is not supported by the oauth2 client service."
		);
	}
	// get access token (for admin account)
	const adminAccount = service.settings.adminAccount;
	if (adminAccount == null) {
		throw new DataConflictError(
			"The operation cannot be completed due to invalid or missing configuration."
		);
	}
	// authorize user and get token
	const authorizeUser = await service.authorize(adminAccount);
	// check if user exists in oauth2 server
	let userExists = await service.getUser(student.user.name, authorizeUser);
	let index = 0;
	while (userExists) {
		index++;
		userExists = await service.getUser(
			student.user.name + index.toString(),
			authorizeUser
		);
	}
	// append the final index, if any
	if (index) {
		student.user.name += index.toString();
		// and also update local user
		const updateLocalUser = {
			id: student.user.id,
			name: student.user.name,
			$state: 2,
		};
		await context.model("User").silent().save(updateLocalUser);
	}
	// get activation code
	const activationCode = event.target.activationCode;
	if (activationCode == null) {
		throw new Error("Activation code is missing.");
	}
	// create user in oauth2 server
	await service.createUser(
		{
			name: student.user.name,
			description: `${student.person.givenName} ${student.person.familyName}`,
			alternateName: student.person.email,
			enabled: true,
			familyName: student.person.familyName,
			givenName: student.person.givenName,
			userCredentials: {
				userPassword: `{clear}${activationCode}`,
				temporary: true
			},
		},
		authorizeUser
	);
	try {
		// send notification for the newly created user
		const mailer = getMailer(context);
		const mailTemplate = await context
			.model("MailConfiguration")
			.where("target")
			.equal("CreateStudentUserAction")
			.silent()
			.getItem();
		// if mail template is null do nothing and exit
		// otherwise send mail
		if (mailTemplate != null) {
			await new Promise((resolve) => {
				mailer
					.template(mailTemplate.template)
					.subject(mailTemplate.subject)
					.to(student.person.email)
					.send(
						{
							model: {
								student,
								action: event.target,
							},
						},
						(err) => {
							if (err) {
								try {
									TraceUtils.error(
										"CreateStudentUserAction",
										"An error occurred while trying to send an email notification for account activation."
									);
									TraceUtils.error(
										"CreateStudentUserAction",
										"Student",
										`${student.id}`,
										"Email Address",
										`${student.person.email || "empty"}`
									);
									TraceUtils.error(err);
								} catch (err1) {
									// do nothing
								}
								return resolve();
							}
							return resolve();
						}
					);
			});
		} else {
			TraceUtils.warn(
				"CreateStudentUserAction",
				"A mail template for account activation notification is missing. User cannot be notified for this action."
			);
		}
	} catch (err) {
		TraceUtils.error(
			"CreateStudentUserAction",
			"An error occurred while trying to send an email notification for account activation."
		);
		TraceUtils.error(err);
	}
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	afterSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			TraceUtils.error(err);
			return callback(err);
		});
}

export class CreateExternalStudentUser extends CreateExternalStudentUserStrategy {
	constructor(app) {
		// install this listener as member of event listeners of CreateStudentUserAction model
		super(app, "CreateStudentUserAction", __filename);
	}
}

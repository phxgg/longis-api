import {ApplicationService} from "@themost/common";
import {ResponseFormatter} from "@themost/express";
import {icsContentType} from "../middlewares/ics";
import {IcsFormatter} from "../formatters/ics.formatter";
import {TraceUtils} from "@themost/common/utils";

export class IcsFormatterService extends ApplicationService {
    /**
     *
     * @param {ExpressDataApplication} app
     */
    constructor(app) {
        super(app);
        if(!app.hasService(ResponseFormatter)){
            app.useService(ResponseFormatter)
        }
        app.getService(ResponseFormatter).formatters.set(icsContentType, IcsFormatter);
    }
}


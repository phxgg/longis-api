import {AttachmentFileSystemStorage} from '@themost/web/files'
import path from 'path';
/**
 * @class
 */
export class PrivateContentService extends AttachmentFileSystemStorage {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        // get physical path from application configuration or use content/private as default path
        super(path.resolve( app.getConfiguration().getSourceAt('settings/universis/content/root') || 'content/private'));
        // set virtual path
        this.virtualPath = '/api/content/private/';
    }
}

import onHeaders from 'on-headers';
import BearerStrategy from 'passport-http-bearer';
import {TraceUtils} from "@themost/common";

/**
 * A middleware which completely disables cache control
 * @returns {Function}
 */
export function noCache() {
    return (req, res, next) => {
        onHeaders(res, ()=> {
            res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
            res.setHeader('Pragma', 'no-cache');
            res.setHeader('Expires', 0);
            // remove express js etag header
            res.removeHeader('ETag');
        });
        return next();
    }
}

class TraceUser {
    constructor(name) {
        this.name = name;
        this.type = Object.getPrototypeOf(this).constructor.name;
    }
    toString() {
        return this.name;
    }
}

class TraceRequest {
    constructor(req) {
        Object.assign(this, {
            method: req.method,
            url: req.url,
            type: Object.getPrototypeOf(this).constructor.name
        });
    }
    toString() {
        return this.url;
    }
}

export class TraceContext {
    /**
     * @param {*} req
     */
    constructor(req) {
        const wrapper = (original) => {
            /**
             * @param {...*} arg
             */
            return (...args) => {
                let username = 'anonymous';
                if (req.context && req.context.interactiveUser) {
                    username = req.context.interactiveUser.name;
                } else if (req.context && req.context.user) {
                    username = req.context.user.name;
                }
                args.unshift(new TraceUser(username), new TraceRequest(req));
                return original.apply(null, args)
            };
        };

        this.log = wrapper(TraceUtils.log);
        this.error = wrapper(TraceUtils.error);
        this.warn = wrapper(TraceUtils.warn);
        this.info = wrapper(TraceUtils.info);
        this.debug = wrapper(TraceUtils.debug);
        this.verbose = wrapper(TraceUtils.verbose);
    } 
}

/**
 * 
 * A middleware that populates the req with a TraceContext util.
 * 
 */
export function addTrace(req, res, next) {
    if (req.context) {
        Object.defineProperty(req.context, 'trace', {
            enumerable: false,
            configurable: true,
            value: new TraceContext(req)
        });
    }

    next();
}


/**
 * @class
 * @description An error for cancelling transaction in testing environments
 */
export class CancelTransactionError extends Error {
    constructor() {
        super();
    }
}

export class TestBearerStrategy extends BearerStrategy {
    /**
     * @param {*=} user
     */
    constructor(user) {
        super({
            passReqToCallback: true,
            realm: 'Users'
        }, (req, token, done) => {
            user = user || {
                "name": "anonymous",
                "authenticationType": "none",
            };
            return done(user);
        });
        this.name = 'bearer';
    }

    authenticate(req) {
        const self = this;
        return self._verify(req, null, (user)=> {
            return self.success(user);
        });
    }
}

export class TestUtils {
    /**
     * Wraps DataAdapter.executeInTransaction() for using in testing environments
     * @param {ExpressDataContext} context
     * @param {Function} func
     * @returns {Promise<any>}
     */
    static executeInTransaction(context, func) {
        return new Promise((resolve, reject) => {
            // clear cache
            const configuration = context.getConfiguration();
            Object.defineProperty(configuration, 'cache', {
                configurable: true,
                enumerable: true,
                writable: true,
                value: { }
            });
            // start transaction
            context.db.executeInTransaction((cb) => {
                try {
                    func().then(() => {
                        return cb(new CancelTransactionError());
                    }).catch( err => {
                        return cb(err);
                    });
                }
                catch (err) {
                    return cb(err);
                }

            }, err => {
                // if error is an instance of CancelTransactionError
                if (err && err instanceof CancelTransactionError) {
                    return resolve();
                }
                if (err) {
                    return reject(err);
                }
                // exit
                return resolve();
            });
        });
    }

    static cancelTransaction() {
        throw new CancelTransactionError();
    }

}

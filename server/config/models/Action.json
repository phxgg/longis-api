{
    "$schema": "https://themost-framework.github.io/themost/models/2018/2/schema.json",
    "@id": "http://schema.org/Action",
    "name": "Action",
    "description": "An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.</p>\n\n<p>See also <a href=\"http://blog.schema.org/2014/04/announcing-schemaorg-actions.html\">blog post</a> and <a href=\"http://schema.org/docs/actions.html\">Actions overview document</a>.",
    "title": "Action",
    "abstract": false,
    "sealed": false,
    "implements": "Thing",
    "version": "1.5",
    "fields": [
        {
            "@id": "http://schema.org/additionalType",
            "name": "additionalType",
            "readonly": true,
            "nullable": false,
            "type": "Text",
            "value": "javascript:return this.model.name;"
        },
        {
            "@id": "http://schema.org/alternateName",
            "name": "alternateName",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/result",
            "name": "result",
            "title": "result",
            "description": "The result produced in the action. e.g. John wrote a book.",
            "type": "Object"
        },
        {
            "@id": "http://schema.org/actionStatus",
            "name": "actionStatus",
            "title": "actionStatus",
            "description": "Indicates the current disposition of the Action.",
            "type": "ActionStatusType",
            "value": "javascript:return { alternateName:'ActiveActionStatus' };",
            "expandable": true
        },
        {
            "@id": "http://schema.org/target",
            "name": "target",
            "title": "target",
            "description": "Indicates a target EntryPoint for an Action.",
            "type": "EntryPoint"
        },
        {
            "@id": "http://schema.org/owner",
            "name": "owner",
            "title": "Owner",
            "description": "The owner of the action e.g. a user who request something.",
            "type": "User",
            "value": "javascript:return this.user();"
        },
        {
            "@id": "http://schema.org/agent",
            "name": "agent",
            "title": "agent",
            "description": "The direct performer or driver of the action (animate or inanimate). e.g. <em>John</em> wrote a book.",
            "type": "User"
        },
        {
            "@id": "http://schema.org/startTime",
            "name": "startTime",
            "title": "startTime",
            "description": "The startTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to start. For actions that span a period of time, when the action was performed. e.g. John wrote a book from <em>January</em> to December.</p>\n\n<p>Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times. This situation may be clarified in future revisions.",
            "type": "DateTime",
            "value": "javascript:return new Date();"
        },
        {
            "@id": "http://schema.org/endTime",
            "name": "endTime",
            "title": "endTime",
            "description": "The endTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to end. For actions that span a period of time, when the action was performed. e.g. John wrote a book from January to <em>December</em>.</p>\n\n<p>Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times. This situation may be clarified in future revisions.",
            "type": "DateTime"
        },
        {
            "@id": "http://schema.org/participant",
            "name": "participants",
            "title": "participants",
            "description": "Other co-agents that participated in the action indirectly. e.g. John wrote a book with <em>Steve</em>.",
            "type": "Account"
        },
        {
            "@id": "http://schema.org/object",
            "name": "object",
            "title": "object",
            "description": "The object upon which the action is carried out, whose state is kept intact or changed. Also known as the semantic roles patient, affected or undergoer (which change their state) or theme (which doesn't). e.g. John read <em>a book</em>.",
            "type": "Object"
        },
        {
            "@id": "http://schema.org/error",
            "name": "error",
            "title": "error",
            "description": "For failed actions, more information on the cause of the failure.",
            "type": "Object"
        },
        {
            "@id": "https://themost.io/schemas/code",
            "name": "code",
            "title": "Action Code",
            "description": "A sequence of alphanumeric characters which represents a random code associated with this this action.",
            "type": "Text",
            "value": "javascript:return this.chars(32);",
            "nullable": false
        },
        {
            "@id": "https://themost.io/schemas/attachments",
            "name": "attachments",
            "description": "A collection of attachments associated with this action.",
            "type": "Attachment",
            "mapping": {
                "associationAdapter": "ActionAttachments",
                "associationType": "junction",
                "cascade": "delete",
                "childModel": "Attachment",
                "childField": "id",
                "parentModel": "Action",
                "parentField": "id",
                "associationObjectField": "object",
                "associationValueField": "value",
                "privileges": [
                    {
                        "mask":15,
                        "type": "global"
                    },
                    {
                        "mask": 15,
                        "type": "global",
                        "account": "Administrators"
                    },
                    {
                        "mask": 1,
                        "type": "self",
                        "filter": "object/owner eq me()"
                    }
                ]
            }
        },
        {
            "@id": "http://themost.io/schemas/assignees",
            "name": "assignees",
            "title": "Assignees",
            "description": "An assignee is a user or group of users responsible for doing the action.",
            "type": "Account"
        },
        {
            "@id": "http://themost.io/schemas/assignees",
            "name": "followers",
            "title": "Followers",
            "description": "A follower is anyone who wants to be kept  in the loop as the action progresses.",
            "type": "Account"
        },
        {
            "@id": "http://schema.org/identifier",
            "name": "identifier",
            "title": "identifier",
            "description": "The identifier property represents any kind of identifier for any kind of Thing, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links.",
            "type": "Guid",
            "value": "javascript:return this.newGuid();"
        }
    ],
    "privileges": [
        {
            "mask": 15,
            "type": "global"
        },
        {
            "mask": 15,
            "type": "global",
            "account": "Administrators"
        },
        {
            "mask": 1,
            "type": "self",
            "account": "Students",
            "filter": "owner eq me()"
        },
        {
            "mask": 1,
            "type": "self",
            "account": "Registrar",
            "filter": "owner eq me()"
        }
    ]
}

import {RequestHandler} from "express";

declare interface PostXlsOptions {
    name: string;
}

export declare function xlsParser(options?: any): RequestHandler;
export declare const XlsxContentType: string;
export declare function xlsPostParser(options?: PostXlsOptions): RequestHandler;